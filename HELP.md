
# WooCommerce Developer Resources
- [developer.woocommerce.com](https://developer.woocommerce.com/)
- [woocommerce.github.io/code-reference](https://woocommerce.github.io/code-reference/)

## sub WooCommerce
- [docs.woocommerce.com](https://docs.woocommerce.com/)
  - [/document/variable-product](https://docs.woocommerce.com/document/variable-product/)

## Storefront
- [woocommerce.com/storefront](https://woocommerce.com/storefront/)
  - [woocommerce.com/storefront-child-theme-themes](https://woocommerce.com/product-category/themes/storefront-child-theme-themes/)
- [docs.woocommerce.com/storefront](https://docs.woocommerce.com/documentation/themes/storefront/)
- Our platform has solid foundations. Based on the popular [Underscores](https://underscores.me/) starter theme used by Automattic for all themes on WordPress.com, it features a responsive layout, a flexible and nestable grid system, and enhanced SEO performance.

# plugins
## payment & shipping
- [klarna.com](https://www.klarna.com/de/verkaeufer/)
  - [klarna.com/woocommerce](https://www.klarna.com/de/verkaeufer/plattformen-und-partner/woocommerce/)

[developers.klarna.com](https://developers.klarna.com/)

- [storefront-powerpack](https://woocommerce.com/products/storefront-powerpack/)
  - 60 €

# WP docs
- [developer.wordpress.org/child-themes](https://developer.wordpress.org/themes/advanced-topics/child-themes/)

## community sites
- [gist.github.com/stuartduff](https://gist.github.com/stuartduff)


## db Umzug
- find: https://demo.fix-clean.eu
- replace: http://wp-cleanshop.lan

```shell
sed -i 's/original/new/g' file.sql
# does not work :(
sed -i 's#https://demo.fix-clean.eu#http://wp-cleanshop.lan#g' file.sql
```
Explanation:

- sed = Stream EDitor
- -i = in-place (i.e. save back to the original file)
- The command string:
  - s = the substitute command
  - original = a regular expression describing the word to replace (or just the word itself)
  - new = the text to replace it with
  - g = global (i.e. replace all and not just the first occurrence)
- file.txt = the file name
