<?php

if ( ! defined( 'MDCLEAN_BASEPATH' ) ) {
	define( 'MDCLEAN_BASEPATH', realpath( __DIR__ . '/view/' ) );
}

add_action( 'wp_enqueue_scripts', 'mdclean_theme_enqueue_styles' );
function mdclean_theme_enqueue_styles() {
	$parenthandle = 'storefront-woocommerce-style';
	$theme        = wp_get_theme();
	wp_enqueue_style( $parenthandle, get_template_directory_uri() . '/assets/css/woocommerce/woocommerce.css',
		[ 'storefront-style', 'storefront-icons' ],  // if the parent theme code has a dependency, copy it to here
		$theme->parent()->get( 'Version' )
	);
	wp_enqueue_style( 'storefront-mdclean', get_stylesheet_uri(),
		[ $parenthandle ],
		$theme->get( 'Version' ) // this only works if you have Version in the style header
	);
	wp_enqueue_style( 'style', get_stylesheet_directory_uri() . '/assets/css/w3schools.css', false, '0.0.1', 'all' );
	wp_enqueue_style( 'style', get_stylesheet_directory_uri() . '/assets/fonts/fontawesome/css/all.css', false, '5.15.1', 'all' );
}

function mdclean_favicon() {
	?>
    <link href="<?= get_stylesheet_directory_uri() ?>/assets/images/favicon.ico" rel="shortcut icon" type="image vnd.microsoft.icon">
	<?php
}

add_action( 'wp_head', 'mdclean_favicon' );

require 'inc/mdclean-functions.php';
require 'inc/storefront/mdclean-functions-overwrites.php';
require 'inc/mdclean-template-hooks.php';
require 'inc/mdclean-template-functions.php';

