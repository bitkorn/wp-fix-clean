<?php


/**
 * Header
 * Hooks in /wp-content/themes/storefront/inc/storefront-template-hooks.php
 * @see  storefront_header_container()
 * @see  storefront_skip_links()
 * @see  storefront_site_branding()
 * @see  storefront_secondary_navigation()
 * @see  storefront_header_container_close()
 * @see  storefront_primary_navigation_wrapper()
 * @see  storefront_primary_navigation()
 * @see  storefront_primary_navigation_wrapper_close()
 */
add_action( 'mbclean_header', 'storefront_header_container', 0 ); // original: 0
add_action( 'mbclean_header', 'storefront_skip_links', 5 ); // original: 5
add_action( 'mbclean_header', 'storefront_site_branding', 20 ); // original: 20
// add_action( 'mbclean_header', 'storefront_secondary_navigation', 30 ); // original: 30
add_action( 'mbclean_header', 'storefront_header_container_close', 41 ); // original: 41
// add_action( 'mbclean_header', 'storefront_primary_navigation_wrapper', 42 ); // original: 42
add_action( 'mbclean_header', 'storefront_primary_navigation', 30 ); // original:
// add_action( 'mbclean_header', 'storefront_primary_navigation_wrapper_close', 68 ); // original: 68


/**
 * Header
 * Hooks in /wp-content/themes/storefront/inc/woocommerce/storefront-woocommerce-template-hooks.php
 * @see storefront_product_search()
 * @see storefront_header_cart()
 */
//add_action( 'mbclean_header', 'storefront_product_search', 40 ); // original: 40
add_action( 'mbclean_header', 'storefront_header_cart', 40 ); // original: 60
