<?php

/**
 * Remove some Storefront actions.
 */
function mdclean_remove_sf_actions() {
	remove_action( 'storefront_header', 'storefront_secondary_navigation', 30 );
	remove_action( 'mbclean_header', 'storefront_secondary_navigation', 30 );
	/**
	 * look at wp-content/themes/storefront/inc/woocommerce/storefront-woocommerce-template-hooks.php
     * @see woocommerce_breadcrumb()
	 */
	remove_action( 'storefront_before_content', 'woocommerce_breadcrumb', 10 );
}

add_action( 'init', 'mdclean_remove_sf_actions' );

/**
 * Top Right Menu
 * https://developer.wordpress.org/reference/functions/wp_nav_menu/
 */
function mdclean_secondary_navigation() {
	if ( has_nav_menu( 'secondary' ) ) {
		?>
        <nav class="mdclean-topright-nav w3-right" role="navigation" aria-label="<?php esc_html_e( 'Secondary Navigation', 'storefront' ); ?>">
			<?php
			wp_nav_menu(
				[
					'theme_location' => 'secondary',
					'fallback_cb'    => '',
				]
			);
			?>
        </nav><!-- #site-navigation -->
		<?php
	}
}

add_action( 'mdclean_topright', 'mdclean_secondary_navigation', 20 );
