<?php

function storefront_header_container() {
	echo '<div class="col-full" style="padding-bottom: 8px">';
}
/**
 * The header container close
 */
function storefront_header_container_close() {
	echo '</div>';
}

/**
 * Fix-Clean Logo HTML
 * Overwrite function in /wp-content/themes/storefront/inc/storefront-template-functions.php
 *
 * @param bool $echo
 *
 * @return string|void
 */
function storefront_site_title_or_logo( $echo = true ) {
	$html = '<div class="w3-hide-small"><br></div>';
	$html .= '<div class="w3-hide-small">';
	$html .= '<a href="' . esc_url( home_url( '/' ) ) . '" rel="home" style="margin-top: 12px;">';
	$html .= '<img src="' . get_stylesheet_directory_uri() . '/assets/images/logo_flat.svg" class="w3-image" alt="logo">';
//	$html .= 'xxx';
	$html .= '</a>';
	$html .= '</div>';
	$html .= '<div class="w3-hide-large w3-hide-medium">';
	$html .= '<a href="' . esc_url( home_url( '/' ) ) . '" rel="home" style="margin-top: 12px;">';
	$html .= '<img src="' . get_stylesheet_directory_uri() . '/assets/images/logo_flat.svg" class="w3-image" alt="logo">';
//	$html .= 'xxx';
	$html .= '</a>';
	$html .= '</div>';
	if ( ! $echo ) {
		return $html;
	}

	echo $html; // WPCS: XSS ok.
}

/**
 * Display Primary Navigation
 *
 * @return void
 * @since  1.0.0
 */
function storefront_primary_navigation() {
	?>
    <nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Primary Navigation', 'storefront' ); ?>">
        <button class="menu-toggle" aria-controls="site-navigation" aria-expanded="false">
            <span><?php echo esc_html( apply_filters( 'storefront_menu_toggle_text', __( 'Menu', 'storefront' ) ) ); ?></span></button>
		<?php
		wp_nav_menu(
			[
				'theme_location'  => 'primary',
				'container_class' => 'primary-navigation',
			]
		);

		wp_nav_menu(
			[
				'theme_location'  => 'handheld',
				'container_class' => 'handheld-navigation',
			]
		);
		?>
    </nav><!-- #site-navigation -->
	<?php
}

/**
 * Display the theme credit
 * Overwrite function in /wp-content/themes/storefront/inc/storefront-template-functions.php
 * @return void
 * @since  1.0.0
 */
function storefront_credit() {
	$links_output = '';

	if ( apply_filters( 'storefront_privacy_policy_link', true ) && function_exists( 'the_privacy_policy_link' ) ) {
		$separator    = '<span role="separator" aria-hidden="true"></span>';
		$links_output = get_the_privacy_policy_link( '', ( ! empty( $links_output ) ? $separator : '' ) ) . $links_output;
	}

	$links_output = apply_filters( 'storefront_credit_links_output', $links_output );
	?>
    <div class="site-info">
		<?php echo esc_html( apply_filters( 'storefront_copyright_text', $content = '&copy; ' . get_bloginfo( 'name' ) . ' ' . date( 'Y' ) ) ); ?>

		<?php if ( ! empty( $links_output ) ) { ?>
            <br/>
			<?php echo wp_kses_post( $links_output ); ?>
		<?php } ?>
    </div><!-- .site-info -->
	<?php
}
